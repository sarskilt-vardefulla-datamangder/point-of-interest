# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en point-of-interest och varje kolumn motsvarar en egenskap för den punkten. 16 attribut är definierade, där de första 6 är obligatoriska. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.

</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**name**](#name)|1|text|**Obligatoriskt** - Anger platsens namn.|
|[**description**](#description)|1|text|**Obligatoriskt** - Anger en kortare beskrivning av platsen. |
|[**id**](#id)|1|heltal|**Obligatoriskt** - Anger en identifierare för platsen.|
|[**type**](#type)|1|[begrepp](#type)|**Obligatoriskt** - Anger typen för platsen. |
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|openinghours|0..1|text| Anger en kortare beskrivning av öppettiderna och om dessa avviker. |
|[opens](#opens)|0..1|[time](#time)| Anger öppningstid enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html).|
|[closes](#closes)|0..1|[time](#time)| Anger stängningstid enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html).|
|[street](#street)|0..1|text|Ange gatuadress.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Ange postnummer.|
|[city](#city)|0..1|text|Ange postort.|
|[phone](#phone)|0..1|text|Ange telefonnumret med inledande landskod, exempelvis +46 |
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om platsen.|
|[image](#image)|0..*|[URL](#url)|Ange en länk eller flera länkar till bilder på platsen.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där platsen presenteras hos den lokala myndigheten eller arrangören.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime**, **time**

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonsegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

Exempel - en plats som öppnar kl 17:00 

</br>
17:00

</br>

## Förtydligande av attribut

### name

Namnet på platsen. Exempelvis "Taxi Stockholm Centralstation".

### description

Beskrivning av platsen.

### type

Typen av point-of-interest kan anges enligt nedan, observera att ni endast ska använda texten "Pharmacy" och inte de utåtgående länkarna. Utåtgående länkar är till för att underlätta arbetet samt tydliggöra definitionen för varje typ av dessa punkter. Då typerna hämtas från klasser i [schema.org](https://schema.org/) kan man ge förslag på ytterligare typer som bör finnas med i nästa version av specifikationen.

[TouristInformationCenter](https://schema.org/TouristInformationCenter) 
<br>

[ATM](https://schema.org/AutomatedTeller)
<br>

[Pharmacy](https://schema.org/Pharmacy)
<br>

[PublicToilet](https://schema.org/PublicToilet)
<br>

[PoliceStation](https://schema.org/PoliceStation)
<br>

[TaxiStand](https://schema.org/TaxiStand)
<br>

[ParkingFacility](https://schema.org/ParkingFacility)
<br>

[TrainStation](https://schema.org/TrainStation)
<br>

[Hospital](https://schema.org/Hospital)
<br>

[EmergencyService](https://schema.org/EmergencyService)
<br>

### latitude

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.


### opens

Beskriver öppningstid för platsen vilket endast sker i vissa fall och då enligt **hh:mm** om ni önskar ange tiden platsen öppnar. För exempelvis Turistinformationscenter är detta ett relevant attribut.  

### closes

Beskriver stängningstid för platsen vilket endast sker i vissa fall och då enligt **hh:mm** om ni önskar ange tiden platsen öppnar. För exempelvis Turistinformationscenter är detta ett relevant attribut.  


### street

Platsens adress.

### postalcode

Postnummer för platsen.

### city

Postort där platsen befinner sig.


### phone 

Telefonnummer till organisationen som ansvarar för verksamheten.

### email

Funktionsadress till organisationen. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: info@organisation.se

### url 

En ingångssida. Anges med inledande schemat **https://** eller **http://** 


### image 

Ange en länk eller flera länkar till bilder på platsen. Anges med inledande schemat **https://** eller **http://** 




 


