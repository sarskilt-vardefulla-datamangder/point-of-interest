# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "id":  "1804-Avesta",
    "name": "Avesta Tourist Information",
    "source": "5567882609",
    "type": "TouristInformationCenter",
    "latitude":  "60.1461619",
    "longitude":  "16.1667696",
    "description":  "Beläget i Avesta bibliotek",
    "openinghours": "Avvikande öppettider under de flesta dagarna",
    "opens": "09:00",
    "closes": "17:00",
    "street":  "Kungsgatan 32",
    "postalcode":  "77430",
    "city":  "Avesta",
    "phone":  "",
    "email":  "info@visitdalarna.se",
    "URL":  "https://www.visitdalarna.se/en/visitorservice?geos=107225",
    "image":  "https://example.com/avesta.jpg"      
}
```
