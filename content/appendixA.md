## Exempel i CSV, kommaseparerad

<div class="example csvtext">
id,name,source,type,latitude,longitude,description,openinghours,opens,closes,street,postalcode,city,phone,email,URL,image
<br>
1804-Avesta,Avesta Tourist Information,5567882609,TouristInformationCenter,60.1461619,16.1667696,Beläget i Avesta bibliotek,Avvikande öppettider under de flesta dagarna,09:00,17:00,Kungsgatan 32,77430,Avesta,,info@visitdalarna.se,https://www.visitdalarna.se/en/visitorservice?geos=107225,https://example.com/avesta.jpg
</div>

