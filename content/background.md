**Bakgrund**

Syftet med denna specifikation är att beskriva information om points of interest på ett enhetligt och standardiserat vis. Specifkationen har baserats på arbetet med informationsmodeller för upplevelser inom besöksnäringen. 

Specifikationen syftar till att ge kommuner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver points of interest. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>



